#!/usr/bin/env perl
use Mojolicious::Lite;
use Mojolicious::Plugin::TtRenderer;

use lib 'lib';

use Try::Tiny;
use App;
use App::DB;
use Data::Dumper;

use App::Model::User;

# constants
use constant CONFIG_FILE => 'config/app.json';

# load config file
my $config = App->parse_json_file( CONFIG_FILE );

App->config( $config );

plugin tt_renderer => { template_options => { DEBUG => 1 } };

# Helpers
helper db => sub { return App::DB->new() };

get '/' => sub {
    my $c = shift;
    $c->render( 'index' );
};

get '/manage_users' => sub {
    my $c = shift;

    my $users = App::Model::User->retrieve_all;

    $c->stash( users => $users );
    $c->render( 'manage_users' );
};

get '/delete_user' => sub {
    my $c = shift;

    my $id_user = $c->param("idUser");

    my $deleted = App::Model::User->delete({
        idUser => $id_user
    });

    $c->render( json => { success => "1" } );

};

get '/show_delete' => sub {
    my $c = shift;
    $c->stash( contenido => "this is the delete" );
    $c->render( 'index' );
};

post 'create_user' => sub {
    my $c = shift;

    my $first_name = $c->param("firstName");
    my $last_name = $c->param("lastName");
    my $birth_date = $c->param("birthDate");
    my $login = $c->param("login");
    my $is_admin = $c->param("isAdmin");

    try {
        my $user = App::Model::User->new(
            firstName => $first_name,
            lastName => $last_name,
            birthDate => $birth_date,
            login => $login,
            password => "ch4ng3m3!",
            isAdmin => $is_admin
        );

        $user->create;
        $c->render( json => { success => 1, message => "User created" } );
    } catch {
        my $error = $_;
        $c->render( json => { error => 1, message => "Unable to create user: " . $error } );
    };
};

post 'save_changes' => sub {
    my $c = shift;
    my $id_user = $c->param("idUser");
    my $first_name = $c->param("firstName");
    my $last_name = $c->param("lastName");
    my $birth_date = $c->param("birthDate");
    my $login = $c->param("login");
    my $is_admin = $c->param("isAdmin");

    try {
        my $user = App::Model::User->update({
            firstName => $first_name,
            lastName => $last_name,
            birthDate => $birth_date,
            login => $login,
            isAdmin => $is_admin
        }, { idUser => $id_user } );

        $c->render( json => { success => 1, message => "User updated" } );

    } catch {
        my $error = $_;
        $c->render( json => { error => 1, message => "User doesn't exist: " . $error } );
    };

};

app->renderer->default_handler( 'tt' );
app->renderer->paths( ['./templates' ] );
app->secrets( [App->secret] );
# Start app
app->start;
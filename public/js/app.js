$('.datepicker').datepicker();

function deleteUser (idUser) {
    $.get( "delete_user", { idUser: idUser } )
        .done(function( data ) {
            removeUserDeleted(idUser);
        }
    );
}

function removeUserDeleted( idUser ) {
    $("#user_id_" + idUser).hide();
}

function createUser() {
        $.post( "create_user", {
            firstName: $("#new_user_first_name").val(),
            lastName: $("#new_user_last_name").val(),
            birthDate: $("#new_user_birth_date").val(),
            login: $("#new_user_login").val(),
            isAdmin: $("#new_user_is_admin").val() == "on" ? 1 : 0
        })
        .done(function( data ) {
            if (data.success) {
                location.reload();
            }
        }
    );
}

function saveChanges( idUser ) {

    // Create field validations

    $.post( "save_changes", {
            idUser: idUser,
            firstName: $("#user_first_name_" + idUser ).val(),
            lastName: $("#user_last_name_" + idUser ).val(),
            birthDate: $("#user_birth_date_" + idUser ).val(),
            login: $("#user_login_" + idUser ).val(),
            isAdmin: $("#user_is_admin_chbx_" + idUser ).is(':checked') ? 1 : 0
        })
        .done(function( data ) {
            cancelModify( idUser );
        }
    );



}

function modifyUser( userId ) {
    enableModify(userId);
}

function cancelModify( userId ){
    disableModify(userId);
}

function enableModify( userId ) {
    $("#user_id_" + userId).removeClass("hide_disabled_look");
    $("#user_id_" + userId).find("input:text").each(function(indx, item ) {
        item.disabled = false;
    })
    $("#user_is_admin_chbx_" + userId).removeAttr("disabled");

    $("#modify_button_" + userId).hide();
    $("#save_button_" + userId).show();
    $("#cancel_button_" + userId).show();
    $("#delete_button_" + userId).hide();
}

function disableModify( userId ) {
    $("#user_id_" + userId).addClass("hide_disabled_look");
    $("#user_id_" + userId).find("input:text").each(function(indx, item ) {
        item.disabled = true;
    })
    $("#user_is_admin_chbx_" + userId).attr("disabled", true);

    $("#modify_button_" + userId).show();
    $("#save_button_" + userId).hide();
    $("#cancel_button_" + userId).hide();
    $("#delete_button_" + userId).show();
}
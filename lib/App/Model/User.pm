package App::Model::User;

use Moose;
use MooseX::ClassAttribute;

extends 'App::DB';

class_has 'store' => (
    is => 'ro',
    isa => 'Str',
    default => 'User'
);

has 'idUser' => (
    is => 'rw',
    isa => 'Num'
);

has 'firstName' => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

has 'lastName' => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

has 'birthDate' => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

has 'login' => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

has 'password' => (
    is => 'rw',
    isa => 'Str',
    required => 1
);

has 'isAdmin' => (
    is => 'rw',
    isa => 'Num'
);

__PACKAGE__->meta->make_immutable();
package App::Exceptions::DB::Create;

use Moose;
extends 'App::Exceptions';
with 'Throwable';
has '+message' => (
    is => 'rw',
    isa => 'Str',
    lazy => 1,
    default => sub { 'create command error (DB)' }
);

__PACKAGE__->meta->make_immutable();
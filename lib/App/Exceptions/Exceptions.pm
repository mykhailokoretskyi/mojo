package App::Exceptions;

use Moose;
with 'Throwable';
has 'message' => ( is => 'rw', isa => 'Str', lazy => 1, default => sub { '' } );

__PACKAGE__->meta->make_immutable();
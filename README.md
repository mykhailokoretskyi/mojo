# What we want to accomplish? #

* This is an educational approach in order to learn the basics of Mojolicious integrated with OOP (on Moose) tied with a MySQL DB.
* It also has a front end side, written in Bootstrap 3

# Prerequisites #

In order to make your life easier, I recommend to use cpan-minus in order to include these packages. This is one "lighter" way than using CPAN:


```
sudo apt-get install cpanminus
```

Once cpanminus is installed, you need to run the following command in order to install the package:


```
cpanm install Mojolicious
```

* Perl packages that are mandatory for this repository to work:
    * Mojolicious
    * Moose
    * MooseX::ClassAttribute
    * Mojolicious::Plugin::TtRenderer
    * SQL::Abstract
    * Time::Piece

# MySQL DB #
I've added a .sql file for you to save some time in generating the DB. Using mysql you can execute this command to easily create the DB:

```
mysql < app.sql
```

# How to start the server #

Once you have Mojolicious installed, then you will have a command on your bash available for you to execute the server:

```
#!bash
morbo app.pl

```
This will execute the server on your localhost at port 3000 -> Open up a browser and hit http://localhost:3000 and you should be DONE!


# Let's make this grow - And make you learn in the process #

* Add another DB driver if possible
* Try to include some unit tests
* Add the ability to change a User PWD
* For UI folks, fix the calendar issue when not modifying a value
* Add input validations
* This is a Mojolicious::Lite example, reach out the mojolicious documentation to make this a Mojolicious app (not a Lite one :) )

# If help? #

* Mojolicious documentation: http://mojolicious.org/perldoc
* CPAN: http://search.cpan.org/
* Ask the author pabloiacovino@gmail.com
* Call Batman